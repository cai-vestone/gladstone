/*
 * meriadoc library
 */

HTMLElement.prototype.delegateEvent = function(eventName, delegateSelector) {
  const delegate = this.querySelector(delegateSelector);
  this.addEventListener(eventName, (event) => {
    if (!delegate.contains(event.target)) {
      event.bubbles = false;
      delegate.dispatchEvent(new event.constructor(event.type, event));
    }
  });
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function loadCSS(url) {
  let link = document.querySelector(`link[href="${url}"]`);

  if (!link) {
    const head = document.querySelector("head");

    link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = url;
    head.appendChild(link);
  }
  link.disabled = false;
}

function disableCSS(url) {
  const link = document.querySelector(`link[href="${url}"]`);

  if (link) {
    link.disabled = true;
  }
}

function loadScript(url) {
  const script = document.createElement("script");

  console.log(`loading script: "${(url)}"`);
  script.src = url;
  script.type = "text/javascript";
  document.querySelector("body").appendChild(script);
}

function unloadScript(url) {
  const script = document.querySelector(`script[src="${url}"]`);

  console.log(`unloading script: "${url}"`);
  if (script) {
    script.remove();
  }
}
