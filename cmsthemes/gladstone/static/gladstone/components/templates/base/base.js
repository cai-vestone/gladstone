/*
 * base page template
 */

"use strict";

window.addEventListener("load", () => {
  const toggle = document.getElementById("nav-toggle");
  const sidebar = document.getElementById(toggle.getAttribute("aria-controls"));
  const mediaQueryList = window.matchMedia("(min-width: 75rem)");

  function resizeHandler(mediaQueryList) {
    if (mediaQueryList.matches) {
      sidebar.show();
    } else {
      sidebar.hide();
    }
  }

  sidebar.show = function() {
    this.classList.remove("pf-m-collapsed");
    this.classList.add("pf-m-expanded");
    document.querySelectorAll(`[aria-controls=${this.id}]`).forEach((control) => {
      control.setAttribute("aria-expanded", "true");
    });
  };

  sidebar.hide = function() {
    this.classList.remove("pf-m-expanded");
    this.classList.add("pf-m-collapsed");
    document.querySelectorAll(`[aria-controls=${this.id}]`).forEach((control) => {
      control.setAttribute("aria-expanded", "false");
    });
  };

  toggle.isExpanded = function() {
    return this.getAttribute("aria-expanded") == "true";
  };

  toggle.addEventListener("click", (event) => {
    const toggle = event.target;
    const sidebar = document.getElementById(toggle.getAttribute("aria-controls"));

    toggle.isExpanded() ? sidebar.hide() : sidebar.show();
  });

  resizeHandler(mediaQueryList);
  mediaQueryList.addListener(resizeHandler);

});
