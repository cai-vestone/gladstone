from setuptools import setup, find_namespace_packages
from ripiu.cmsplugin_filer_svg import __version__

setup(
    name='cmsthemes.gladstone',
    version=__version__,
    url='https://gitlab.com/cai-vestone/gladstone',
    license='BSD-new',
    description='A django-cms theme using Patternfly',
    long_description=open('README.rst').read(),
    author='matteo vezzola',
    author_email='matteo@caivestone.it',
    packages=find_namespace_packages(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'Django >= 2.2',
    ],
    python_requires='>=3.7',
    include_package_data=True,
    zip_safe=False,
)
